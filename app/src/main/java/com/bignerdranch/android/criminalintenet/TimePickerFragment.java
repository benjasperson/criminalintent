package com.bignerdranch.android.criminalintenet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import java.util.Date;

/**
 * Created by benjamin.jasperson on 1/10/2018.
 */

public class TimePickerFragment extends AppCompatDialogFragment {
    private static final String ARG_TIME = "time";
    public static final String EXTRA_TIME =
            "com.bignerdranch.android.criminalintent.time";
    private TimePicker mTimePicker;

    public static TimePickerFragment newInstance(Date date) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TIME, date);

        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Date date = (savedInstanceState == null ?
                (Date) getArguments().getSerializable(ARG_TIME) :
                (Date) savedInstanceState.getSerializable(ARG_TIME));


            View v = LayoutInflater.from(getActivity())
                    .inflate(R.layout.dialog_time, null);

            return new AlertDialog.Builder(getActivity())
                    .setView(v)
                    .setTitle(R.string.time_picker_title)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mTimePicker = v.findViewById(R.id.dialog_time_picker);
                                    long time;
                                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        time = (mTimePicker.getHour() * 60 + mTimePicker.getMinute()) * 60000;
                                    } else {
                                        time = (mTimePicker.getCurrentHour() * 60 + mTimePicker.getCurrentMinute()) * 60000;
                                    }
                                    sendResult(Activity.RESULT_OK, time);
                                }
                            })
                    .create();

    }

    private void sendResult(int resultCode, long time) {
        if (getTargetFragment() == null) {
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_TIME,time);

        getTargetFragment()
                .onActivityResult(getTargetRequestCode(),resultCode, intent);
    }
}
