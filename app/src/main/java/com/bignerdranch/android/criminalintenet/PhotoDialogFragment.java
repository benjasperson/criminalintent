package com.bignerdranch.android.criminalintenet;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;

/**
 * Created by benjamin.jasperson on 1/30/2018.
 */

public class PhotoDialogFragment extends AppCompatDialogFragment {
    private static final String ARG_PHOTO_FILE = "photo_file";

    private File mPhotoFile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mPhotoFile = (File)getArguments().getSerializable(ARG_PHOTO_FILE);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_dialog,container,false);

        ImageView photoView = view.findViewById(R.id.fragment_image_view);
        if (mPhotoFile == null || !mPhotoFile.exists()){
            photoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(
                    mPhotoFile.getPath(),getActivity());
            photoView.setImageBitmap(bitmap);
        }
        return view;
    }

    public static PhotoDialogFragment newInstance(File file){
        Bundle args = new Bundle();
        args.putSerializable(ARG_PHOTO_FILE,file);

        PhotoDialogFragment fragment = new PhotoDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
