package com.bignerdranch.android.criminalintenet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by benjamin.jasperson on 1/11/2018.
 */

public class DatePickerActivity extends SingleFragmentActivity {
    private static final String EXTRA_CRIME_ID=
            "com.bignerdranch.android.criminalintent.crime_id";
    private UUID crimeId;

    @Override
    protected Fragment createFragment() {
        crimeId = (UUID) getIntent()
                .getSerializableExtra(EXTRA_CRIME_ID);

        DatePickerFragment dialog = DatePickerFragment
                .newInstance(CrimeLab.get(this).getCrime(crimeId).getDate(),false);
        return dialog;
    }

    public static Intent newIntent(Context packageContext,UUID crimeId){
        Intent intent = new Intent(packageContext,DatePickerActivity.class);
        intent.putExtra(EXTRA_CRIME_ID, crimeId);
        return intent;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
