package com.bignerdranch.android.criminalintenet.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.bignerdranch.android.criminalintenet.Crime;

import java.util.Date;
import java.util.UUID;

import database.CrimeDbSchema;
import database.CrimeDbSchema.CrimeTable;

/**
 * Created by benjamin.jasperson on 1/23/2018.
 */

public class CrimeCursorWrapper extends CursorWrapper {
    public CrimeCursorWrapper(Cursor cursor){
        super(cursor);
    }

    public Crime getCrime() {
        String uuidString = getString(getColumnIndex(CrimeTable.Cols.UUID));
        String title = getString(getColumnIndex(CrimeTable.Cols.TITLE));
        long date = getLong(getColumnIndex(CrimeTable.Cols.DATE));
        int isSolved = getInt(getColumnIndex(CrimeTable.Cols.SOLVED));
        int isRequiresPolice = getInt(getColumnIndex(CrimeTable.Cols.POLICE));
        String suspect = getString(getColumnIndex(CrimeTable.Cols.SUSPECT));

        Crime crime = new Crime(UUID.fromString(uuidString));
        crime.setTitle(title);
        crime.setDate(new Date(date));
        crime.setSolved(isSolved != 0);
        crime.setRequiresPolice(isRequiresPolice != 0);
        crime.setSuspect(suspect);

        return crime;
    }
}
