package com.bignerdranch.android.criminalintenet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by benjamin.jasperson on 1/10/2018.
 */

public class DatePickerFragment extends AppCompatDialogFragment {
    public static final String EXTRA_DATE =
            "com.bignerdranch.android.criminalintent.date";

    private static final String ARG_DATE = "date";
    private static final String ARG_IS_TABLET = "isTablet";

    private DatePicker mDatePicker;
    private boolean mIsTablet;
    private Date mDate;
    private int year;
    private int month;
    private int day;
    private View v;

    public static DatePickerFragment newInstance(Date date,boolean isTablet) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DATE, date);
        args.putBoolean(ARG_IS_TABLET, isTablet);

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setIsTablet(savedInstanceState);
        if(mIsTablet) {
            setFragment(savedInstanceState);
            return new AlertDialog.Builder(getActivity())
                    .setView(v)
                    .setTitle(R.string.date_picker_title)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    sendResult(Activity.RESULT_OK);
                                }
                            })
                    .create();
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setIsTablet(savedInstanceState);
        if(mIsTablet){
            return super.onCreateView(inflater,container,savedInstanceState);
        } else {
            setFragment(savedInstanceState);

            Button okButton = v.findViewById(R.id.ok_button);
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            });

            return v;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(ARG_IS_TABLET,mIsTablet);
        outState.putSerializable(ARG_DATE,mDate);
        super.onSaveInstanceState(outState);
    }

    private void setIsTablet(@Nullable Bundle savedInstanceState) {
        mIsTablet = savedInstanceState == null ?
                getArguments().getBoolean(ARG_IS_TABLET):
                savedInstanceState.getBoolean(ARG_IS_TABLET);
    }


    private void sendResult(int resultCode) {
        int year = mDatePicker.getYear();
        int month = mDatePicker.getMonth();
        int day = mDatePicker.getDayOfMonth();
        Date date = new GregorianCalendar(year, month, day).getTime();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATE, date);

        if(getTargetFragment() == null){
            getActivity().setResult(resultCode,intent);
        } else {
            getTargetFragment()
                    .onActivityResult(getTargetRequestCode(), resultCode, intent);
        }
    }

    private void setFragment(Bundle savedInstanceState){
        mDate = (savedInstanceState == null?
                (Date) getArguments().getSerializable(ARG_DATE) :
                (Date) savedInstanceState.getSerializable(ARG_DATE));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mDate);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        v = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_date, null);

        mDatePicker = v.findViewById(R.id.dialog_date_picker);
        mDatePicker.init(year, month, day, null);
    }
}
